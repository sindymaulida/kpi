-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 08 Jan 2021 pada 16.13
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pegawai`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `absensi_karyawan`
--

CREATE TABLE `absensi_karyawan` (
  `id` int(11) NOT NULL,
  `nik` int(11) DEFAULT NULL,
  `nama` varchar(255) NOT NULL,
  `absen` int(11) NOT NULL,
  `hadir` int(11) NOT NULL,
  `tidak_hadir` int(11) NOT NULL,
  `izin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `absensi_karyawan`
--

INSERT INTO `absensi_karyawan` (`id`, `nik`, `nama`, `absen`, `hadir`, `tidak_hadir`, `izin`) VALUES
(1, 14210028, 'Kharisma Maulana', 1, 1, 0, 1),
(3, NULL, 'Salwa', 0, 0, 0, 1),
(4, NULL, 'kakasimwah', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `alasan_karyawan`
--

CREATE TABLE `alasan_karyawan` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alasan` varchar(255) NOT NULL,
  `tanggal` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `alasan_karyawan`
--

INSERT INTO `alasan_karyawan` (`id`, `nama`, `alasan`, `tanggal`) VALUES
(1, 'Kharisma Maulana Pasaribu', 'Sedang Sakit', '22/05/2020'),
(2, 'Salwa', 'xbjsjhux', '07/01/2021');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_karyawan`
--

CREATE TABLE `data_karyawan` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `salary` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `handphone` varchar(255) NOT NULL,
  `nik` varchar(255) NOT NULL,
  `tentang` varchar(255) NOT NULL,
  `image_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `data_karyawan`
--

INSERT INTO `data_karyawan` (`id`, `name`, `position`, `age`, `start_date`, `salary`, `email`, `handphone`, `nik`, `tentang`, `image_name`) VALUES
(1, 'Kakasimwah', 'Admin', 23, '5/01/2021', 8000000, 'kakasimwah127@gmail.com', '082314476403', '12345', 'Saya Adalah Founder Matadeveloper', 'Kharisma_Maulana.jpg'),
(2, 'Salwa', 'Sekretaris', 17, '18/09/19', 1000, 'nawrah.putri22@gmail.com', '089624561206', '220802', 'xvgsvh', 'Memilih_Hijab_Cornskin_Ini_Saran_Produsen_Hijab_di_MalangJlXDI.jpg'),
(3, 'kakasimwah', 'manager', 20, '19/12/2020', 1000, 'nawrah.putri22@gmail.com', '134576789', '36768789', 'wfegrtytrytry', 'Mein_Lesejahr_2020_-_Highlights,_Flops_und_Statistiken_+_Template.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `history_karyawan`
--

CREATE TABLE `history_karyawan` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `info` varchar(255) NOT NULL,
  `tanggal` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `history_karyawan`
--

INSERT INTO `history_karyawan` (`id`, `nama`, `info`, `tanggal`) VALUES
(1, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '22/05/2020 14:00:02'),
(2, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '22/05/2020 14:00:03'),
(3, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '22/05/2020 14:00:03'),
(4, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '22/05/2020 14:00:04'),
(5, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu telah melakukan absen', '22/05/2020 14:00:05'),
(6, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '22/05/2020 14:00:05'),
(7, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '22/05/2020 14:00:41'),
(8, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '22/05/2020 14:00:43'),
(9, 'Admin', 'Admin telah melakukan login', '22/05/2020 14:00:54'),
(10, 'Admin', 'Admin telah melakukan login', '22/05/2020 14:02:55'),
(11, 'Admin', 'Admin telah melakukan login', '22/05/2020 14:07:19'),
(12, 'Admin', 'Admin telah melakukan login', '22/05/2020 14:07:26'),
(13, 'Admin', 'Admin telah melakukan login', '22/05/2020 14:11:05'),
(14, 'Admin', 'Admin telah melakukan login', '22/05/2020 14:49:59'),
(15, 'Admin', 'Admin telah melakukan login', '22/05/2020 14:51:43'),
(16, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '22/05/2020 14:56:04'),
(17, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '22/05/2020 14:56:41'),
(18, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '22/05/2020 14:56:42'),
(19, 'Admin', 'Admin telah melakukan login', '07/01/2021 14:02:42'),
(20, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '07/01/2021 14:03:50'),
(21, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '07/01/2021 14:06:06'),
(22, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '07/01/2021 14:06:26'),
(23, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '07/01/2021 14:06:31'),
(24, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '07/01/2021 14:06:34'),
(25, 'Admin', 'Admin telah melakukan login', '07/01/2021 14:06:51'),
(26, 'Admin', 'Admin telah melakukan login', '07/01/2021 14:06:55'),
(27, 'Admin', 'Admin telah melakukan login', '07/01/2021 14:08:52'),
(28, 'Admin', 'Admin telah melakukan login', '07/01/2021 14:42:13'),
(29, 'Admin', 'Admin telah melakukan login', '07/01/2021 14:44:13'),
(30, 'Admin', 'Admin telah melakukan login', '08/01/2021 02:11:59'),
(31, 'Admin', 'Admin telah melakukan login', '08/01/2021 02:12:04'),
(32, 'Admin', 'Admin telah melakukan login', '08/01/2021 02:13:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `setting_absensi`
--

CREATE TABLE `setting_absensi` (
  `id` int(11) NOT NULL,
  `mulai_absen` varchar(255) NOT NULL,
  `selesai_absen` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `setting_absensi`
--

INSERT INTO `setting_absensi` (`id`, `mulai_absen`, `selesai_absen`) VALUES
(1, '14:00', '15:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_absensi`
--

CREATE TABLE `tbl_absensi` (
  `transid` int(11) UNSIGNED NOT NULL,
  `nik` varchar(255) DEFAULT NULL,
  `waktu` time DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tbl_absensi`
--

INSERT INTO `tbl_absensi` (`transid`, `nik`, `waktu`, `tanggal`) VALUES
(1, '14210028', '14:00:05', '2020-05-22 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users_karyawan`
--

CREATE TABLE `users_karyawan` (
  `id` int(11) NOT NULL,
  `nik` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `users_karyawan`
--

INSERT INTO `users_karyawan` (`id`, `nik`, `password`, `level`) VALUES
(1, '14210027', 'admin', 'Admin'),
(2, '14210028', 'maulana', 'Karyawan'),
(3, '220802', 'salwa', 'Karyawan'),
(4, '36768789', 'iwa', 'Karyawan');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `absensi_karyawan`
--
ALTER TABLE `absensi_karyawan`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `alasan_karyawan`
--
ALTER TABLE `alasan_karyawan`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `data_karyawan`
--
ALTER TABLE `data_karyawan`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `history_karyawan`
--
ALTER TABLE `history_karyawan`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `setting_absensi`
--
ALTER TABLE `setting_absensi`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `tbl_absensi`
--
ALTER TABLE `tbl_absensi`
  ADD PRIMARY KEY (`transid`) USING BTREE;

--
-- Indeks untuk tabel `users_karyawan`
--
ALTER TABLE `users_karyawan`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `absensi_karyawan`
--
ALTER TABLE `absensi_karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `alasan_karyawan`
--
ALTER TABLE `alasan_karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `data_karyawan`
--
ALTER TABLE `data_karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `history_karyawan`
--
ALTER TABLE `history_karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT untuk tabel `setting_absensi`
--
ALTER TABLE `setting_absensi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_absensi`
--
ALTER TABLE `tbl_absensi`
  MODIFY `transid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users_karyawan`
--
ALTER TABLE `users_karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
