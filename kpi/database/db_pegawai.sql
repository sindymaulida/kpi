/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 100140
 Source Host           : localhost:3307
 Source Schema         : db_pegawai

 Target Server Type    : MySQL
 Target Server Version : 100140
 File Encoding         : 65001

 Date: 22/05/2020 14:56:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for absensi_karyawan
-- ----------------------------
DROP TABLE IF EXISTS `absensi_karyawan`;
CREATE TABLE `absensi_karyawan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nik` int(11) NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `absen` int(11) NOT NULL,
  `hadir` int(11) NOT NULL,
  `tidak_hadir` int(11) NOT NULL,
  `izin` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of absensi_karyawan
-- ----------------------------
INSERT INTO `absensi_karyawan` VALUES (1, 14210028, 'Kharisma Maulana Pasaribu', 1, 1, 0, 1);

-- ----------------------------
-- Table structure for alasan_karyawan
-- ----------------------------
DROP TABLE IF EXISTS `alasan_karyawan`;
CREATE TABLE `alasan_karyawan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alasan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of alasan_karyawan
-- ----------------------------
INSERT INTO `alasan_karyawan` VALUES (1, 'Kharisma Maulana Pasaribu', 'Sedang Sakit', '22/05/2020');

-- ----------------------------
-- Table structure for data_karyawan
-- ----------------------------
DROP TABLE IF EXISTS `data_karyawan`;
CREATE TABLE `data_karyawan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `position` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `age` int(11) NOT NULL,
  `start_date` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `salary` int(11) NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `handphone` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nik` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tentang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `image_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of data_karyawan
-- ----------------------------
INSERT INTO `data_karyawan` VALUES (1, 'Kharisma Maulana Pasaribu', 'Admin', 23, '29/03/2018', 8000000, 'kharismamaulana1@gmail.com', '082282845087', '14210028', 'Saya Adalah Founder Matadeveloper', 'Kharisma_Maulana.jpg');

-- ----------------------------
-- Table structure for history_karyawan
-- ----------------------------
DROP TABLE IF EXISTS `history_karyawan`;
CREATE TABLE `history_karyawan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `info` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of history_karyawan
-- ----------------------------
INSERT INTO `history_karyawan` VALUES (1, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '22/05/2020 14:00:02');
INSERT INTO `history_karyawan` VALUES (2, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '22/05/2020 14:00:03');
INSERT INTO `history_karyawan` VALUES (3, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '22/05/2020 14:00:03');
INSERT INTO `history_karyawan` VALUES (4, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '22/05/2020 14:00:04');
INSERT INTO `history_karyawan` VALUES (5, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu telah melakukan absen', '22/05/2020 14:00:05');
INSERT INTO `history_karyawan` VALUES (6, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '22/05/2020 14:00:05');
INSERT INTO `history_karyawan` VALUES (7, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '22/05/2020 14:00:41');
INSERT INTO `history_karyawan` VALUES (8, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '22/05/2020 14:00:43');
INSERT INTO `history_karyawan` VALUES (9, 'Admin', 'Admin telah melakukan login', '22/05/2020 14:00:54');
INSERT INTO `history_karyawan` VALUES (10, 'Admin', 'Admin telah melakukan login', '22/05/2020 14:02:55');
INSERT INTO `history_karyawan` VALUES (11, 'Admin', 'Admin telah melakukan login', '22/05/2020 14:07:19');
INSERT INTO `history_karyawan` VALUES (12, 'Admin', 'Admin telah melakukan login', '22/05/2020 14:07:26');
INSERT INTO `history_karyawan` VALUES (13, 'Admin', 'Admin telah melakukan login', '22/05/2020 14:11:05');
INSERT INTO `history_karyawan` VALUES (14, 'Admin', 'Admin telah melakukan login', '22/05/2020 14:49:59');
INSERT INTO `history_karyawan` VALUES (15, 'Admin', 'Admin telah melakukan login', '22/05/2020 14:51:43');
INSERT INTO `history_karyawan` VALUES (16, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '22/05/2020 14:56:04');
INSERT INTO `history_karyawan` VALUES (17, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '22/05/2020 14:56:41');
INSERT INTO `history_karyawan` VALUES (18, 'Kharisma Maulana Pasaribu', 'Kharisma Maulana Pasaribu Telah melakukan login', '22/05/2020 14:56:42');

-- ----------------------------
-- Table structure for setting_absensi
-- ----------------------------
DROP TABLE IF EXISTS `setting_absensi`;
CREATE TABLE `setting_absensi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mulai_absen` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `selesai_absen` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of setting_absensi
-- ----------------------------
INSERT INTO `setting_absensi` VALUES (1, '06:00', '24:00');

-- ----------------------------
-- Table structure for tbl_absensi
-- ----------------------------
DROP TABLE IF EXISTS `tbl_absensi`;
CREATE TABLE `tbl_absensi`  (
  `transid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nik` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `waktu` time(0) NULL DEFAULT NULL,
  `tanggal` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`transid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_absensi
-- ----------------------------
INSERT INTO `tbl_absensi` VALUES (1, '14210028', '14:00:05', '2020-05-22 00:00:00');

-- ----------------------------
-- Table structure for users_karyawan
-- ----------------------------
DROP TABLE IF EXISTS `users_karyawan`;
CREATE TABLE `users_karyawan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `level` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users_karyawan
-- ----------------------------
INSERT INTO `users_karyawan` VALUES (1, '14210027', 'admin', 'Admin');
INSERT INTO `users_karyawan` VALUES (2, '14210028', 'maulana', 'Karyawan');

SET FOREIGN_KEY_CHECKS = 1;
