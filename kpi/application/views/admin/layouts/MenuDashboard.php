                    <div class="row">
                    	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    		<div class="card bg-dark influencer-profile-data">
                    			<div class="card-body">
                    				<div class="row">
                    					<div class="col-xl-2 col-lg-4 col-md-4 col-sm-4 col-12">
                    						<div class="text-center">
                    							<img src="<?= base_url(); ?>assets/images/logos.png" alt="User Avatar"
                    								class="rounded-circle user-avatar-xxl">
                    						</div>
                    					</div>
                    					<div class="col-xl-10 col-lg-8 col-md-8 col-sm-8 col-12">
                    						<div class="user-avatar-info">
                    							<div class="m-b-20">
                    								<div class="user-avatar-name">
                    									<h2 class="mb-1 text-white"></h2>
                    								</div>
                    							</div>
                    							<div class="user-avatar-address">
                    								<p class="border-bottom pb-3">
                    									<span class="d-xl-inline-block d-block mb-2"><i
                    											class="fa fa-map-marker-alt mr-2 text-primary "></i>Grojogan Wirokerten Banguntapan Bantul Yogyakarta</span>
                    								</p>
                    								<div class="mt-3">
														<p>Fokus Pendidikan
                                                                 Dalam pendidikan IT, kami membagi menjadi beberapa keahlian. Banyak keahlian yang ada di Pondok IT. Diantaranya adalah Pondok Programmer, Pondok Multimedia, Pondok Imers, Pondok Cyber, dan masih banyak lagi. <a href="https://pondokit.com/">PONDOK IT</a> Pendidikan yang menyediakan spendidikan gratis</p>
                    								</div>
                    							</div>
                    						</div>
                    					</div>
                    				</div>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end content  -->
                    <!-- ============================================================== -->
                 <!--    <div class="row">
                    	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    		<div class="row">
                    			<div class="col-6 col-lg-6 col-md-6">
                    				<div class="card bg-dark influencer-profile-data">
                    					<div class="card-body">
                    						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam dolorem aspernatur fugit enim quae corrupti minus possimus, maiores omnis excepturi laboriosam, error placeat quam provident quasi temporibus pariatur quos veritatis!</p>
                    					</div>
                    				</div>
                    			</div>
								<div class="col-6 col-lg-6 col-md-6">
                    				<div class="card bg-dark influencer-profile-data">
                    					<div class="card-body">
                    						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam dolorem aspernatur fugit enim quae corrupti minus possimus, maiores omnis excepturi laboriosam, error placeat quam provident quasi temporibus pariatur quos veritatis!</p>
                    					</div>
                    				</div>
                    			</div>
                    		</div>
                    	</div>
                    </div> -->
                    </div>
                    </div>
                    </div>
